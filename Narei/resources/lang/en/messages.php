<?php
return [
    'lang' => 'ENG',
    'members' => 'MEMBERS',
    'forms' => 'FORMS',
    'news&events' => 'NEWS & EVENTS',
    'statistics' => 'STATISTICS',
    'text_main_button' => 'LEARN MORE',
    'title' => 'NAREI Moldova - investment, development and construction',
    'name_brand' => 'NAREI',
    'name_brand_!' => 'NAREI!',
    'country' => 'MOLDOVA',
    'text_header_1' => 'A community of investors, developers, constructors, brokers, consultants, experts, owners and mentors in the real estate sector.',

    'text_advantage_title_main' => 'Fostering successful and long-term investments in Moldova',
    'text_advantage_title_1' => 'The goal of the Association is to connect companies and physical persons of the',
    'text_advantage_title_2' => 'Real Estate Industry with the following targets.',
    'text_advantage_1' => 'Initiate and conduct round tables on current problems of the industry, together with representatives of legislative and executive authorities.',
    'text_advantage_2' => 'Come up with legislative initiatives in the investment and construction sectors - to provide legal assistance to members of NAREI, for an independent examination of investment projects.',
    'text_advantage_3' => 'Deliver access to specialized market researches, expert reports, studies, articles.',
    'text_advantage_4' => 'Help members to apply for financing and real estate related services.',
    'text_advantage_5' => 'Participate in international events - MIPIM, Expo Real and local and regional Real Estate & Investment Forums and other Real Estate events, networking.',
    'text_advantage_6' => 'Organize effective communication platforms for member promotion, news, trends.',
    'text_advantage_7' => 'Connect with potential Real Estate Investors.',

    'events' => 'EVENTS:',
    'news' => 'NEWS:',
    'see_all' => 'SEE ALL',
    'text_events_1' => 'Real Estate Forum',
    'text_events_2' => 'Happy birthday to Chisinau',
    'text_news_1' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    'text_news_2' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',

    'text_member_1' => 'Become a',
    'text_member_2' => 'member',
    'text_member_3' => 'of ',
    'text_member_button' => 'JOIN NOW',

    'contact_us' => 'CONTACT US',
    'join' => 'JOIN',
    'about' => 'ABOUT',

];
