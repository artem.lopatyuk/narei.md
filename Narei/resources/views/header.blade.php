<div @yield('header') class="container">

    @yield('header-index-start')

    <img @yield('logo-header-index') class="logo-header" @yield('logo-header-svg-index') src="img/logo-header.svg" width="171" height="75" align="left" alt="logo">
    <ul @yield('nav-header-index') class="nav-header">
        <li @yield('nav-item-header-index') class="nav-item-header">
            <a href="/" @yield('nav-lang-index') class="nav-lang">{{ __('messages.lang') }}</a>
        </li>
        <li @yield('nav-item-header-index') class="nav-item-header">
            <a href="/members" @yield('nav-link-header-index') class="nav-link-header">{{ __('messages.members') }}</a>
        </li>
        <li @yield('nav-item-header-index') class="nav-item-header">
            <a href="/forms" @yield('nav-link-header-index') class="nav-link-header">{{ __('messages.forms') }}</a>
        </li>
        <li @yield('nav-item-header-index') class="nav-item-header">
            <a href="/news&events" @yield('nav-link-header-index') class="nav-link-header">{{ __('messages.news&events') }}</a>
        </li>
        <li @yield('nav-item-header-index') class="nav-item-header">
            <a href="/statistics" @yield('nav-link-header-index') class="nav-link-header">{{ __('messages.statistics') }}</a>
        </li>
    </ul>

    @yield('header-index-end')

</div>






