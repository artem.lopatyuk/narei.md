@extends('layout')

@section('title')
    {{ __('messages.title') }}
@endsection

@section('name_page')
    CONTACT US
@endsection

@section('header')
    class="container header"
@endsection

@section('header-strip')
    <div class="strip-box">
        <img class="header-strip" src="img/header-strip.svg" width="100px" height="100%" align="left"
             alt="header-strip">
        <h1 class="text-header-strip">@yield('name_page')</h1>
        <div class="header-strip-box"></div>
    </div>
@endsection

@section('main_content')

    @yield('header-strip')


    <div class="map">
        <iframe class="map-img" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d87073.64305361488!2d28.788136614162003!3d46.999964773815044!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40c97dcd90f46f3b%3A0x8d30b610ab97780e!2z0J3QsNGG0LjQvtC90LDQu9GM0L3Ri9C5INCc0YPQt9C10Lkg0K3RgtC90L7Qs9GA0LDRhNC40Lgg0Lgg0JXRgdGC0LXRgdGC0LLQtdC90L3QvtC5INCY0YHRgtC-0YDQuNC4!5e0!3m2!1sru!2s!4v1657549917691!5m2!1sru!2s" width="2000" height="560" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>

    <div class="contact-us">
        <h1 class="text-h1-contact-us">We would love to hear from you</h1>
        <div class="contacts">
            <div class="location">
                <img class="location-img" src="img/location.svg" width="69px" height="69px" align="left"
                     alt="location-img">
                <div class="contacts-content">
                    <h2 class="text-h2-contact-us">LOCATION</h2>
                    <p class="text-contact-us">2/1 Porumbitei street, Chisinau</p>
                </div>
            </div>
            <div class="phone">
                <img class="phone-img" src="img/phone.svg" width="69px" height="69px" align="left" alt="phone-img">
                <div class="contacts-content">
                    <h2 class="text-h2-contact-us">PHONE NUMBER</h2>
                    <p class="text-contact-us">+373 694 14 041</p>
                </div>
            </div>
        </div>
    </div>
    <div class="form">
        <div class="form-content">
            <h1 class="text-h1-form">Please fill out your information request below.</h1>
            <h3 class="text-h3-form">NAME</h3>
            <div class="form-name">
                <div class="form-first-name">
                    <input class="input-form" type="text">
                    <p class="text-form">FIRST NAME</p>
                </div>
                <div class="form-last-name">
                    <input class="input-form" type="text">
                    <p class="text-form">LAST NAME</p>
                </div>
            </div>
            <div class="form-radio-box">
                <h3 class="text-h3-form">Are you a REALTOR?</h3>
                <p class="text-form"><input class="radio-form" type="radio">YES</p>
                <p class="text-form"><input class="radio-form" type="radio">OTHER:</p>
                <input class="input-form" type="text">
            </div>

            <div class="form-box">
                <div class="form-box-content">
                    <h3 class="text-h3-form">PHONE NUMBER</h3>
                    <input class="input-form" type="text">
                </div>
                <div class="form-box-content">
                    <h3 class="text-h3-form">EMAIL</h3>
                    <input class="input-form" type="text">
                </div>
                <div class="form-box-content">
                    <h3 class="text-h3-form">DOMAIN</h3>
                    <input class="input-form" type="text">
                </div>
                <div class="form-box-comment">
                    <div class="form-box-content">
                        <h3 class="text-h3-form">COMMENTS</h3>
                        <input class="input-form-big" type="text">
                    </div>
                    <div class="form-box-button">
                        <button class="button-form">SUBMIT</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
