<div class="container box-footer">
    <img class="logo-footer" src="img/logo-footer.svg" width="285" height="125" align="left"
         alt="logo">
    <ul class="nav-footer">
        <li class="nav-item-footer"><a href="/contact-us" class="nav-link-footer"><nobr>{{ __('messages.contact_us') }}</nobr></a></li>

       <li class="nav-item-footer"><a href="/?" class="nav-link-footer">{{ __('messages.join') }}</a></li> {{-- приведет к формам--}}

        <li class="nav-item-footer"><a href="#" class="nav-link-footer">{{ __('messages.about') }}</a></li>
    </ul>
    <ul class="ul-social-footer">
        <li class="li-social-footer">
            <a href="/linkedin"><img class="" src="img/linkedin.svg" width="45" height="45" alt="linkedin"></a>
        </li>
        <li class="li-social-footer">
            <a href="/facebook"><img class="" src="img/facebook.svg" width="45" height="45" alt="facebook"></a>
        </li>
    </ul>
</div>
