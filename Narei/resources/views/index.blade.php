@extends('layout')

@section('title')
    {{ __('messages.title') }}
@endsection

@section('logo-header-svg-index')
    src="img/logo-header-index.svg"
@endsection

@section('logo-header-index')
    class="logo-header-index"
@endsection

@section('nav-header-index')
    class="nav-header-index"
@endsection

@section('nav-item-header-index')
    class="nav-item-header-index"
@endsection

@section('nav-link-header-index')
    class="nav-link-header-index"
@endsection

@section('nav-lang-index')
    class="nav-lang-index"
@endsection

@section('header-index-start')
    <img class="img-header-index" src="img/img-01.png" width="777px" height="1098px" align="right"
         alt="photo-background">
    <div class="background-header-index">
        @endsection

        @section ('header-index-end')
            <div class="text-header-box">
                <h1 class="text-header-h1">{{ __('messages.name_brand') }}<br>{{ __('messages.country') }}</h1>
                <h2 class="text-header-h2">{{ __('messages.text_header_1') }}</h2>
                <button class="button-header-box">{{ __('messages.text_main_button') }}</button>
            </div>
            <ul class="ul-pagination-header">
                <li class="li-pagination-header">
                    <div class="pagination-header-active"></div>
                </li>
                <li class="li-pagination-header">
                    <div class="pagination-header"></div>
                </li>
                <li class="li-pagination-header">
                    <div class="pagination-header"></div>
                </li>
                <li class="li-pagination-header">
                    <div class="pagination-header"></div>
                </li>
            </ul>
        @endsection

        @section('main_content')

            <div class="container advantage">
                <h2 class="text-advantage-h2">{{ __('messages.text_advantage_title_main') }}</h2>
                <p class="text-advantage">{{ __('messages.text_advantage_title_1') }}
                    <br>{{ __('messages.text_advantage_title_2') }}</p>
                <ul class="ul-advantage">
                    <li class="li-advantage">
                        <img class="img-advantage" src="img/check-mark.svg" width="100" height="100" alt="check-mark">
                        <p id="text-advantage-1" class="text-advantage">{{ __('messages.text_advantage_1') }}</p>
                    </li>
                    <li class="li-advantage-centered">
                        <img class="img-advantage" src="img/check-mark.svg" width="100" height="100" alt="check-mark">
                        <p id="text-advantage-2"
                           class="text-advantage-centered">{{ __('messages.text_advantage_2') }}</p>
                    </li>
                    <li class="li-advantage">
                        <img class="img-advantage" src="img/check-mark.svg" width="100" height="100" alt="check-mark">
                        <p id="text-advantage-3" class="text-advantage">{{ __('messages.text_advantage_3') }}</p>
                    </li>
                </ul>
                <ul class="ul-pagination-advantage">
                    <li class="li-pagination-advantage">
                        <div id="pagination-pagination-advantage-1" class="pagination-advantage-active"></div>
                    </li>
                    <li class="li-pagination-advantage">
                        <div id="pagination-pagination-advantage-2" class="pagination-advantage"></div>
                    </li>
                    <li class="li-pagination-advantage">
                        <div id="pagination-pagination-advantage-3" class="pagination-advantage"></div>
                    </li>
                    <li class="li-pagination-advantage">
                        <div id="pagination-pagination-advantage-4" class="pagination-advantage"></div>
                    </li>
                    <li class="li-pagination-advantage">
                        <div id="pagination-pagination-advantage-5" class="pagination-advantage"></div>
                    </li>
                </ul>
            </div>

            <div class="container events_news">
                <div class="events">
                    <h3 class="text-events-news-h3">{{ __('messages.events') }}</h3>
                    <a href="/events" class="link-events-news">{{ __('messages.see_all') }}</a>
                    <div class="box-events-news">
                        <img class="img-events-news" src="img/img-02.png" width="589px" height="258px" align="left"
                             alt="photo-events-01">
                        <a href="/events" class="text-events-news">{{ __('messages.text_events_1') }}</a>
                        <div class="strip-horizontal-events-news"></div>
                    </div>
                    <div class="box-events-news">
                        <img class="img-events-news" src="img/img-03.png" width="589px" height="258px" align="left"
                             alt="photo-events-02">
                        <a href="/news" class="text-events-news">{{ __('messages.text_events_2') }}</a>
                        <div class="strip-horizontal-events-news"></div>
                    </div>
                </div>
                <div class="news">
                    <h3 class="text-events-news-h3">{{ __('messages.news') }}</h3>
                    <a href="/news" class="link-events-news">{{ __('messages.see_all') }}</a>
                    <div class="box-events-news">
                        <img class="img-events-news" src="img/img-02.png" width="589px" height="258px" align="left"
                             alt="photo-news-01">
                        <a href="/news" class="text-events-news">{{ __('messages.text_news_1') }}</a>
                        <div class="strip-horizontal-events-news"></div>
                    </div>
                    <div class="box-events-news">
                        <img class="img-events-news" src="img/img-02.png" width="589px" height="258px" align="left"
                             alt="photo-news-02">
                        <a href="/news" class="text-events-news">{{ __('messages.text_news_2') }}</a>
                        <div class="strip-horizontal-events-news"></div>
                    </div>
                </div>
            </div>

            <div class="container members">
                <img class="img-member" src="img/img-05.png" width="1190px" height="562px" align="right"
                     alt="photo-member-background">
                <div class="background-member">
                    <div class="text-member-box">
                        <h2 class="text-member-h2">
                            <nobr>{{ __('messages.text_member_1') }}</nobr>
                            <br>
                            <nobr>{{ __('messages.text_member_2') }}<br>{{ __('messages.text_member_3') }}
                                <strong>{{ __('messages.name_brand_!') }}</strong></nobr>
                        </h2>
                        <button class="button-member-box">
                            <nobr>{{ __('messages.text_member_button') }}</nobr>
                        </button>
                    </div>
                </div>
            </div>

@endsection
