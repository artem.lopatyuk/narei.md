<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/header.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/contact-us.css">
    <title>@yield('title')</title>
</head>

<body>

<header>
@include('header')
</header>

@yield('main_content')

<footer>
    @include('footer')
</footer>


<script type="text/javascript">
    var text_advantage_1 = "<?php echo __('messages.text_advantage_1'); ?>"
    var text_advantage_2 = "<?php echo __('messages.text_advantage_2'); ?>"
    var text_advantage_3 = "<?php echo __('messages.text_advantage_3'); ?>"
    var text_advantage_4 = "<?php echo __('messages.text_advantage_4'); ?>"
    var text_advantage_5 = "<?php echo __('messages.text_advantage_5'); ?>"
</script>

<script src="js/script.js"></script>

</body>

</html>
