@if(app()->getLocale() == 'ro')
    <a href="/ro" class="nav-lang lang-ro">
                <span>
                   {{ __('messages.lang') }}
                </span>
    </a>
@else
    <a href="/en" class="nav-lang lang-en">
                <span>
                   {{ __('messages.lang') }}
                </span>
    </a>
@endif


