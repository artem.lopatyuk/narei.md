const pagination_1 = document.querySelector('#pagination-pagination-advantage-1')
const pagination_2 = document.querySelector('#pagination-pagination-advantage-2')
const pagination_3 = document.querySelector('#pagination-pagination-advantage-3')
const pagination_4 = document.querySelector('#pagination-pagination-advantage-4')
const pagination_5 = document.querySelector('#pagination-pagination-advantage-5')

const text_1 = document.querySelector('#text-advantage-1')
const text_2 = document.querySelector('#text-advantage-2')
const text_3 = document.querySelector('#text-advantage-3')

console.log(pagination_1)

function onClick(node, text1, text2, text3, color, color_active) {
    node.onclick = () => {
        pagination_1.style.backgroundColor = color
        pagination_2.style.backgroundColor = color
        pagination_3.style.backgroundColor = color
        pagination_4.style.backgroundColor = color
        pagination_5.style.backgroundColor = color
        addStyle(node, text1, text2, text3, color, color_active)
    }
}

const addStyle = function (node, text1, text2, text3, color, color_active) {
    text_1.textContent = text1
    text_2.textContent = text2
    text_3.textContent = text3
    node.style.backgroundColor = color
    node.style.backgroundColor = color_active
}

onClick(pagination_1, text_advantage_1, text_advantage_2, text_advantage_3, '#97a6a6', '#2f4e4d')
onClick(pagination_2, text_advantage_2, text_advantage_3, text_advantage_4, '#97a6a6', '#2f4e4d')
onClick(pagination_3, text_advantage_3, text_advantage_4, text_advantage_5, '#97a6a6', '#2f4e4d')
onClick(pagination_4, text_advantage_4, text_advantage_5, text_advantage_1, '#97a6a6', '#2f4e4d')
onClick(pagination_5, text_advantage_5, text_advantage_1, text_advantage_2, '#97a6a6', '#2f4e4d')


